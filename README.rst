OpenPLC Editor
==================

IDE capable of creating programs for the OpenPLC Runtime



Pre-Requisites
------------------------

For linux the following packages are required:

.. code-block:: console

    sudo apt-get install build-essential pkg-config bison flex autoconf automake libtool make libssl-dev python2.7 python-pip python-wxgtk3.0

For windows:

.. code-block:: console

    ?todo?


Install and run from pypi
---------------------------

.. code-block:: console

    # install
    pip install openplc-editor

    # run
    openplc-editor.py


Find "OpenPLC Editor" on the applications menu and launch it

Install and run git
------------------------

.. code-block:: console

    # get the code
    git clone https://gitlab.com/openplcproject/openplc_editor.git
    cd openplc_editor

    # install
    ./install.sh

    # run
    openplc-editor.py




