#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT="$(dirname "$DIR")"

#$ROOT/docs/gen_api_rst.py
sphinx-build -a $ROOT/docs $ROOT/public
